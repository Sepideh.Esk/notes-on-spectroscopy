# Final target.
spectrum.pdf: %.pdf: %.tex
	pdflatex -halt-on-error -shell-escape $<

# Clean up.
clean:
	rm -f *.aux *.log *.pdf
