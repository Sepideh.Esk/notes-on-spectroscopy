Notes for spectroscopy
----------------------

Copyright (C) 2022 Sepideh Eskandarlou <sepideh.eskandarlou@gmail.com>\
See the end of the file for license conditions.

In this project, I keep my notes about spectroscopy in LaTeX.
To run this project run the command below:

```shell
make
```




### Copyright information

This project is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This project is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this project.  If not, see <https://www.gnu.org/licenses/>.
